package com.example.calculus_2;

import android.content.res.Configuration;
import android.widget.TextView;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    TextView workTextView, historyTextView;
    Button button41;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        try{
            super.onCreate(savedInstanceState);
            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            {
                // setTitle("Портретная ориентация");

                setContentView(R.layout.activity_main);
            }

            else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            {
                //setTitle("Альбомная ориентация");

                setContentView(R.layout.activity_land);


            }
        }
        catch (Exception ex)
        {

        }




        workTextView = findViewById(R.id.workTextView);
        historyTextView = findViewById(R.id.historyTextView);
        button41 = findViewById(R.id.button41);
    }


    String str, newStr, action = "none", Act = "a";
    Double a, b, c;
    Integer numClick, i;
    boolean point;




    //Кнопка 0
    public void zeroButtonClick(View view) {

        str = workTextView.getText().toString();


        if (str == "")
        {
            workTextView.setText("0");
        }
        else
        {
            if (str == action)
            {
                workTextView.setText("0");

                historyTextView.setText(historyTextView.getText() + " " +  action);

                Act = "b";
            }
            else
            {
                numClick = Integer.parseInt(str);


                if (numClick != 0)
                {
                    workTextView.setText(str + "0");
                }
            }

        }
    }



    //Кнопки 1-9
    public void numButtonClick(View view)
    {
        str = workTextView.getText().toString();

        Button button = (Button)view;

        numClick = Integer.parseInt(button.getText().toString());


//        switch (Act)
//        {
//            case "a":
//                {
//
//                }
//                break;
//        }





        if (str == "" || str == "0"  || str == action)
        {
            if (Act == "action")
            {
                if (str == "Sin()" || str == "Cos()" || str == "Tg()" || str == "Ctg()")
                {
                    historyTextView.setText(action);

                    workTextView.setText(numClick.toString());
                }
                else
                {
                    historyTextView.setText(a.toString() + " " + action + " ");
                }
            }


            workTextView.setText(numClick.toString());

            Act = "b";
        }
        else
        {
            workTextView.setText(str + numClick.toString());
        }
    }




    //Удаление последнего символа строки
    public void backspaceClick(View view)
    {
        str = workTextView.getText().toString();

        if (str != "")
        {
            if (str.length() == 1)
            {
                workTextView.setText("");
            }
            else
            {
                newStr = "";

                newStr += str.substring(0, (str.length() - 1));

                workTextView.setText(newStr);

            }
        }
    }


    //Очистка всей строки
    public void clearClick(View view)
    {
        workTextView.setText("");
    }



    //Очистка всех действий
    public void clearAllClick(View view)
    {
        historyTextView.setText("");
        workTextView.setText("");

        action = "none";
        Act = "a";
    }




    //Кнопки сложения, вычитания, умножения, деления
    public void actionClick(View view)
    {
        str = workTextView.getText().toString();


        if (str == "")
        {
            historyTextView.setText("0");

            a = 0.0;

            Button button = (Button)view;
            action = button.getText().toString();

            workTextView.setText(action);
        }
        else
        {
            switch (str)
            {
                case "+": case "-": case "/": case "*":
            {

            } break;

                default:
                {
                    if (Act == "a")
                    {
                        historyTextView.setText(str);
                    }
                    a = Double.parseDouble(str);
                }
            }

            Button button = (Button)view;
            action = button.getText().toString();

            workTextView.setText(action);

        }

        Act = "action";
    }



    // Вывод результата
    public void rezultClick(View view)
    {
        str = workTextView.getText().toString();

        if (Act == "b" && str != action && str != "")
        {
            b = Double.parseDouble(str);


            switch (action)
            {
                case "+": c = a + b; break;
                case "-": c = a - b; break;
                case "/": c = a / b; break;
                case "*": c = a * b; break;
                case "Sin()": c = Math.sin(b);  break;
                case "Cos()": c = Math.cos(b);  break;
                case "Tg()": c = Math.tan(b);  break;
                case "Ctg()": c = (1 / Math.tan(b)); break;
            }

            if (action == "+" || action == "-" || action == "/" || action == "*")
            {
                historyTextView.setText(historyTextView.getText() + " " + b.toString() + " =");
            }
            else
            {

                historyTextView.setText(action.substring(0, action.length() - 1) + " " + b.toString() + ") =");
            }


            workTextView.setText(c.toString());

            Act = "a";
        }

    }


    // Кнопка точки (дробное число)
    public void pointClick(View view)
    {
        str = workTextView.getText().toString();

        if (str == "")
        {
            workTextView.setText("0.");
        }
        else
        {
            if (str == action)
            {
                historyTextView.setText(a.toString() + " " + action + " ");

                workTextView.setText("0.");
            }
            else
            {
                point = false;

                for (i = 0; i < str.length(); i++)
                {
                    if (str.indexOf('.') != -1)
                    {
                        point = true;

                        break;
                    }
                }


                if (point == false)
                {
                    workTextView.setText(workTextView.getText() + ".");
                }
            }
        }
    }



    public void configurationChanged (Configuration newConfiguration)
    {

    }


    // Кнопки синуса, косинуса, тангенса, котангенса

    public void functionButtonClick(View view)
    {
        str = workTextView.getText().toString();


        historyTextView.setText("");

        Button button = (Button)view;
        action = button.getText().toString();

        workTextView.setText(action);

        Act = "b";



    }
}






